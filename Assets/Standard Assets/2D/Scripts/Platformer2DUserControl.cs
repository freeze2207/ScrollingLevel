using System;
using UnityEngine;
//using UnityStandardAssets.CrossPlatformInput;

namespace UnityStandardAssets._2D
{
    [RequireComponent(typeof (PlatformerCharacter2D))]
    public class Platformer2DUserControl : MonoBehaviour
    {
        private PlatformerCharacter2D m_Character;
        public float moveableLength = 17.7f;                    // Length of the whole moveable area
        public float minX = 0.1f;                               // X , Y axis limits
        public float minY = 0.2f;
        public float maxY = 1.0f;

        public float jumpModifier = 0.0f;
        private float distanceX = 0.0f;
        private float movedOnX = 0.0f;
        private float movedOnY = 0.0f;
        private float startX = 0.0f;

        private void Awake()
        {
            m_Character = GetComponent<PlatformerCharacter2D>();
        }

        private void Update()
        {
            jumpModifier = 0.0f;

            if (Input.touchCount == 1)
            {
                Touch touch = Input.GetTouch(0);

                switch (touch.phase)
                {
                    case TouchPhase.Began:
                        movedOnX = 0.0f;
                        movedOnY = 0.0f;
                        distanceX = 0.0f;
                        break;

                    case TouchPhase.Moved:
                        movedOnX += Vector2.Dot(touch.deltaPosition, Vector2.right);
                        movedOnY += Vector2.Dot(touch.deltaPosition, Vector2.up);
                        break;

                    case TouchPhase.Ended:
                        if (Math.Abs(movedOnX) >= Screen.width * 0.1)
                        {
                            distanceX = Mathf.Clamp(movedOnX, Screen.width * -1, Screen.width);
                        }
                        if (movedOnY >= Screen.height * 0.1)
                        {
                            jumpModifier = Mathf.Clamp(movedOnY / (0.5f * Screen.height), minY, maxY);
                        }
                        startX = gameObject.transform.position.x;
                        
                        break;

                    case TouchPhase.Canceled:
                        break;

                    default:
                        break;
                }
            }

            float objectRatio = Mathf.Abs(gameObject.transform.position.x - startX) / moveableLength;
            float directionalScreenRatio = distanceX / Screen.width;

            // Limit the move value
            if (Mathf.Abs(directionalScreenRatio) <= objectRatio)
            {
                directionalScreenRatio = 0;
            }
            else
            {
                if (directionalScreenRatio > 0 && directionalScreenRatio < minX)
                {
                    directionalScreenRatio = minX;
                }

                if (directionalScreenRatio < 0 && directionalScreenRatio > -1 * minX)
                {
                    directionalScreenRatio = -1 * minX;
                }
            }

            m_Character.Move(directionalScreenRatio, false, jumpModifier > 0, jumpModifier);
        }
    }
}
