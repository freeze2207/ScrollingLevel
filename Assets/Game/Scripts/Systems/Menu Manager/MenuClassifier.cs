using UnityEngine;

[CreateAssetMenu(fileName = "MenuClassifier", menuName = "Create/Menu Classifier")]
public class MenuClassifier : ScriptableObject
{
	public string menuName;
}
