using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class MenuManager : Singleton<MenuManager>
{
	public enum GameStates
    {
		game_normal,
		game_pause
    }

	public GameStates gameStates = GameStates.game_normal;

	[System.Serializable]
	public class GameStateChangedEvent : UnityEvent<MenuManager.GameStates> { }
	public GameStateChangedEvent GameStateChanged;

	private Dictionary<string, Menu> MenuList = new Dictionary<string, Menu>();

	public T GetMenu<T>(MenuClassifier menuClassifier) where T : Menu
	{
		Menu menu;
		if (MenuList.TryGetValue(menuClassifier.menuName, out menu))
		{
			return (T)menu;
		}
		return null;
	}

	public void AddMenu(Menu menu)
	{
		if (MenuList.ContainsKey(menu.menuClassifier.menuName))
		{
			Debug.Assert(false, $"Menu is already registered {menu.menuClassifier.menuName}");
		}
		MenuList.Add(menu.menuClassifier.menuName, menu);
	}

	public void Remove(Menu menu)
	{
		MenuList.Remove(menu.menuClassifier.name);
	}

	// You can overload this as often as you want

	public void ShowMenu(MenuClassifier classifier, string options = "")
	{
		Menu menu;
		if (MenuList.TryGetValue(classifier.name, out menu))
		{
			menu.OnShowMenu(options);
		}
	}

	public void HideMenu(MenuClassifier classifier, string options = "")
	{
		Menu menu;
		if (MenuList.TryGetValue(classifier.name, out menu))
		{
			menu.OnHideMenu(options);
		}
	}
}
