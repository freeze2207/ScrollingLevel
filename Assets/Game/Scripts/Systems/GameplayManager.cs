using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameplayManager : MonoBehaviour
{
    public MenuClassifier hudMenuClassifier;
    public UnityStandardAssets._2D.Platformer2DUserControl userControl;
    public UnityStandardAssets._2D.PlatformerCharacter2D userCharacter;
    private HUDMenuController hudController;

    public void Start()
    {
        MenuManager.Instance.GameStateChanged.AddListener(OnGameStatesChanged);
        hudController = MenuManager.Instance.GetMenu<HUDMenuController>(hudMenuClassifier);
    }

    public void OnGameStatesChanged(MenuManager.GameStates _state)
    {
        switch (_state)
        {
            case MenuManager.GameStates.game_normal:
                userControl.enabled = true;
                break;
            case MenuManager.GameStates.game_pause:
                userControl.enabled = false;
                break;
            default:
                break;
        }
    }

    private void Update()
    {
        hudController.moveText.text = "Movement: " + string.Format("{0:N3}", userControl.gameObject.GetComponent<Rigidbody2D>().velocity.x);
        if (userControl.jumpModifier != 0)
        {
            hudController.jumpText.text = "Jump: " + string.Format("{0:N3}", userControl.jumpModifier * userCharacter.m_JumpForce);
        }
    }
}
