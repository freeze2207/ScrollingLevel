using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SettingsMenuController : Menu
{
	public SceneReference gameScene;
	public MenuClassifier hudMenuClassifier;
	public MenuClassifier mainMenuClassifier;

	public void OnClickReturnBtn()
	{
		MenuManager.Instance.HideMenu(this.menuClassifier);
		MenuManager.Instance.GameStateChanged.Invoke(MenuManager.GameStates.game_normal);
	}

	public void OnClickQuitBtn()
	{
		SceneLoader.Instance.UnloadScene(gameScene);
		MenuManager.Instance.HideMenu(this.menuClassifier);
		MenuManager.Instance.HideMenu(hudMenuClassifier);
		MenuManager.Instance.ShowMenu(mainMenuClassifier);
	}
}
