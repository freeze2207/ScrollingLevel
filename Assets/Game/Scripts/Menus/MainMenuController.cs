using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MainMenuController : Menu
{
	public SceneReference mainMenuScene;
	public SceneReference gameplayScene;
	public MenuClassifier hudMenuClassifier;

	public void OnClickPlayButton()
	{
		SceneLoader.Instance.LoadScene(gameplayScene);
		MenuManager.Instance.ShowMenu(hudMenuClassifier);
		MenuManager.Instance.HideMenu(this.menuClassifier);
	}
}
