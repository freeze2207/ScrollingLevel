using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class HUDMenuController : Menu
{
    public MenuClassifier settingsMenuClassifier;
    public Text moveText;
    public Text jumpText;

    public void OnClickPauseBtn()
    {
        MenuManager.Instance.ShowMenu(settingsMenuClassifier);
        MenuManager.Instance.GameStateChanged.Invoke(MenuManager.GameStates.game_pause);
    }
}
