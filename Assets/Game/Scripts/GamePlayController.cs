using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GamePlayController : MonoBehaviour
{
    public float death_y = -9.0f;
    public AudioClip deathSound;
    public AudioClip jumpSound;
    public Camera mainCamera;
    public AudioSource audio;
    public GameObject player;

    [SerializeField]
    private GameObject playerSpawnSpot;
    [SerializeField]
    private GameObject playerCameraPosition;

    private float resetDelay;
    private float cameraMin = 0;
    private float cameraMax = 48.79f;


    // Start is called before the first frame update
    void Start()
    {
        audio = gameObject.GetComponent<AudioSource>();
        resetDelay = deathSound.length;
        ResetPlayer();
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Space))
        {
            audio.PlayOneShot(jumpSound);
        }
        if (player.transform.position.y < death_y)
        {
            OnPlayerDeath();
            Invoke("ResetPlayer", resetDelay);
        }

        MoveCamera();
    }

    private void ResetPlayer()
    {
        player.transform.position = playerSpawnSpot.transform.position;
        player.transform.rotation = playerSpawnSpot.transform.rotation;
        
        player.GetComponent<Rigidbody2D>().constraints = RigidbodyConstraints2D.FreezeRotation;
    }

    private void OnPlayerDeath()
    {
        player.GetComponent<Rigidbody2D>().constraints = RigidbodyConstraints2D.FreezePositionY;
        player.transform.position += new Vector3(0, 0.1f);
        audio.PlayOneShot(deathSound);
    }

    private void MoveCamera()
    {
        if (mainCamera.transform.position.x != playerCameraPosition.transform.position.x)
        {
            mainCamera.transform.position += new Vector3((playerCameraPosition.transform.position.x - mainCamera.transform.position.x) * Time.deltaTime * 5, 0, 0);

        }
        if (mainCamera.transform.position.x < cameraMin)
        {
            mainCamera.transform.position = new Vector3(cameraMin, 0, -10);
        }
        if (mainCamera.transform.position.x > cameraMax)
        {
            mainCamera.transform.position = new Vector3(cameraMax, 0, -10);
        }
    }
}
