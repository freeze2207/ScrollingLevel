using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CollisionController : MonoBehaviour
{
    public GameObject player;
    public Text winText;
    private float delay;

    [SerializeField]
    private GamePlayController gamePlayController;
    [SerializeField]
    private AudioClip soundEffect;

    private void Start()
    {
        if (gamePlayController.player != null)
        {
            player = gamePlayController.player;
        }
        delay = soundEffect.length;
    }

    public void OnTriggerEnter2D(Collider2D collision)
    {
        gamePlayController.audio.PlayOneShot(soundEffect);
        if (gameObject.name == "Flag")
        {
            winText.text = "Grats! You Made it!";
            player.GetComponent<Rigidbody2D>().constraints = RigidbodyConstraints2D.FreezeAll;
            Invoke("OnTouchFlag", delay);
        }
        if (gameObject.name == "Flower")
        {
            player.GetComponent<Rigidbody2D>().constraints = RigidbodyConstraints2D.FreezeAll;
            Invoke("OnTouchFlower", delay);
        }
    }

    private void OnTouchFlower()
    {
        player.transform.localScale *= 1.2f;
        player.transform.position += new Vector3(0, 0.3f, 0);
        Destroy(gameObject);
        player.GetComponent<Rigidbody2D>().constraints = RigidbodyConstraints2D.FreezeRotation;
    }

    private void OnTouchFlag()
    {
        Application.Quit();
    }
}
